class MatchingsController < ApplicationController
  # GET /matchings
  # GET /matchings.json
  def index
    @matchings = Matching.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @matchings }
    end
  end

  # GET /matchings/1
  # GET /matchings/1.json
  def show
    @matching = Matching.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @matching }
    end
  end

  # GET /matchings/new
  # GET /matchings/new.json
  def new
    @matching = Matching.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @matching }
    end
  end

  # GET /matchings/1/edit
  def edit
    @matching = Matching.find(params[:id])
  end

  # POST /matchings
  # POST /matchings.json
  def create
    @matching = Matching.new(params[:matching])

    respond_to do |format|
      if @matching.save
        format.html { redirect_to @matching, notice: 'Matching was successfully created.' }
        format.json { render json: @matching, status: :created, location: @matching }
      else
        format.html { render action: "new" }
        format.json { render json: @matching.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /matchings/1
  # PUT /matchings/1.json
  def update
    @matching = Matching.find(params[:id])

    respond_to do |format|
      if @matching.update_attributes(params[:matching])
        format.html { redirect_to @matching, notice: 'Matching was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @matching.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /matchings/1
  # DELETE /matchings/1.json
  def destroy
    @matching = Matching.find(params[:id])
    @matching.destroy

    respond_to do |format|
      format.html { redirect_to matchings_url }
      format.json { head :no_content }
    end
  end
end
