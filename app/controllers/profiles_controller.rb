class ProfilesController < ApplicationController
  include ProfilesHelper
  include HarversineDistance
  # GET /profiles
  # GET /profiles.json
  # def index
  #   @profiles = nil

  #   respond_to do |format|
  #     format.html # index.html.erb
  #     format.json { render json: @profiles }
  #   end
  # end

  # GET /profile/:user_id
  # GET /profile/:user_id.json
  def show
    @user = User.find(params[:id])
    @profile = {:user => @user, :bio => @user.biography, :interests => @user.interests}
    

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @profile }
    end
  end

  # # GET /profiles/new
  # # GET /profiles/new.json
  # def new
  #   @profile = Profile.new

  #   respond_to do |format|
  #     format.html # new.html.erb
  #     format.json { render json: @profile }
  #   end
  # end

  # # GET /profiles/1/edit
  # def edit
  #   @profile = Profile.find(params[:id])
  # end

  # POST /profiles/create
  # POST /profiles/create.json
  def create
    @user = User.new(
                    :username => params[:username],
                    :email => params[:email],
                    :password => params[:password],
                    :password_confirmation => params[:password],
                    :first_name => params[:name].split(' ').first,
                    :last_name => params[:name].split(' ').last
                    )

    @biography = Biography.new(
                              :about => params[:about],
                              :birthday => parse_date(params[:birthday]),
                              :occupation => params[:occupation],
                              :photo => params[:photo]
                            )

    @location = Location.create(:longitude=> params[:longitude], :latitude=> params[:latitude])

    respond_to do |format|
      if @user.save
        @biography.user_id = @user.id
        @location.user_id = @user.id
        @location.save!
        if @biography.save
          @profile = {:user => @user, :bio => @biography}
          
          format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
          format.json { render json: @profile, status: :created, location: @profile }
        else
          @user.destroy
          @profile = {:errors => @biography.errors.to_json}
          
          format.html { render action: "new" }
          format.json { render json: @profile[:errors], status: :unprocessable_entity }
        end
      else
        @profile = {:errors => @user.errors.to_json}
        
        format.html { render action: "new" }
        format.json { render json: @profile[:errors], status: :unprocessable_entity }
      end
    end
  end

  # GET /profile/1/match.json
  def match
    @results = []
    @all_matches = []

    @user = User.find(params[:id])

    if @user
      # getting all the matches from all interests
      @user.interests.each do |interest|
        matches = User.joins(:interests).where(:interests => {:name => interest.name})

        matches.each do |match|
          @all_matches.push match unless( match.email == @user.email or match.location == nil)
        end
      end

      # removing duplicate matches
      @all_matches = @all_matches.uniq if @all_matches

      # filtering matches by distance
      @all_matches.each do |match|
        distance = haversine_distance(
                                      @user.location.latitude, 
                                      @user.location.longitude, 
                                      match.location.latitude, 
                                      match.location.longitude)

        match_hash = {}

        if distance["mi"] <= 30
          match_hash[:username]    = match.username
          match_hash[:first_name]  = match.first_name
          match_hash[:last_name]   = match.last_name
          match_hash[:email]       = match.email
          match_hash[:updated_at]  = match.updated_at
          match_hash[:created_at]  = match.created_at
          match_hash[:about]       = match.biography.about
          match_hash[:occupation]  = match.biography.occupation
          match_hash[:distance_away] = distance["mi"]
          match_hash[:photo] = match.biography.photo
          match_hash[:id] = match.id

          @results.push match_hash
        end
      end
      
      @results = @results.uniq if @results

      respond_to do |format|
        format.json { render json: @results }
      end
    end
  end

  # POST /profile/1/update_interests.json
  def update_interests
    @interest_ids = params[:interests]
    @interest_ids = @interest_ids.split(',').uniq
    @interest_ids = @interest_ids.map(&:to_i) #converting to integer
    @interests = Interest.find(@interest_ids)

    @user = User.find(params[:id])
    if @user
      @user.interests = @interests
    else
      false
    end
    respond_to do |format|
      format.json { render json: @interests }
    end
  end

  # POST /profile/1/update_location.json
  def update_location
    @user = User.find(params[:id])
    if @user
      @user.location.longitude = params[:longitude]
      @user.location.latitude = params[:latitude]
      @user.location.save!
    end
    respond_to do |format|
      format.json { render json: @user.location }
    end
  end

  # # PUT /profiles/1
  # # PUT /profiles/1.json
  # def update
  #   @profile = Profile.find(params[:id])

  #   respond_to do |format|
  #     if @profile.update_attributes(params[:profile])
  #       format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
  #       format.json { head :no_content }
  #     else
  #       format.html { render action: "edit" }
  #       format.json { render json: @profile.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /profiles/1
  # # DELETE /profiles/1.json
  # def destroy
  #   @profile = Profile.find(params[:id])
  #   @profile.destroy

  #   respond_to do |format|
  #     format.html { redirect_to profiles_url }
  #     format.json { head :no_content }
  #   end
  # end
end
