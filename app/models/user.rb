class User < ActiveRecord::Base
  include Rhoconnect::Resource

  has_secure_password
  has_one :biography
  has_one :location
  
  has_many :matchings
  has_many :matches, :through => :matchings

  has_many :messages
  has_and_belongs_to_many :interests

  validates_presence_of :email, :username, :password_confirmation, :on => :create
  validates_uniqueness_of :email, :username
  attr_accessible :email, :first_name, :last_name, :password, :password_confirmation, :provider, :username

  def partition
    lambda { self.username }
  end

  def self.rhoconnect_query(partition)
    user = User.where(:email => partition)
  end
end
