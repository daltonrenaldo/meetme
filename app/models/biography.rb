class Biography < ActiveRecord::Base
  include Rhoconnect::Resource
  belongs_to :user
  has_many :images
  validates_presence_of :birthday
  attr_accessible :about, :birthday, :occupation, :photo, :user_id

  def partition 
    lambda { self.user.username }
  end

  def self.rhoconnect_query(partition)
    user = User.find_by_email(partition)
    Biography.where(:user_id => user.id)
  end
end
