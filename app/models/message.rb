class Message < ActiveRecord::Base
  include Rhoconnect::Resource

  attr_accessible :message, :recipient_deleted, :recipient_id, :user_deleted, :user_id
  belongs_to :user
  belongs_to :recipient, :class_name => "User"

  def partition 
    lambda { self.user.username }
  end

  def self.rhoconnect_query(partition)
    user = User.find_by_email(partition)
    Message.where("(user_id = ? OR recipient_id = ?)", user.id.to_s, user.id.to_s)
  end
end
