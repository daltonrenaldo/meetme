class Location < ActiveRecord::Base
  include Rhoconnect::Resource
  
  belongs_to :user
  attr_accessible :latitude, :longitude, :name

  def partition
    lambda { self.user.username }
  end

  def self.rhoconnect_query(partition)
    user = User.find_by_email(partition)
    Location.where("(user_id = ?)", user.id.to_s)
  end
end
