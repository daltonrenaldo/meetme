class Image < ActiveRecord::Base
 #  include Rhoconnect::Resource
  
  attr_accessible :biography_id, :filename
  belongs_to :biography

	def initialize(args = nil)       # Establish the connection with s3.
	  AWS::S3::Base.establish_connection!(
	    :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
	    :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
	  )
	  @baseurl = 'http://s3.amazonaws.com/'  # Set up the base url.
	  @bucket = 'meetme-upload'      # Set up the bucket.
	  super(args)
	end

	def query(params=nil)
	  @result={}
	  AWS::S3::Bucket.find(@bucket).objects.each do |entry|
	    # Get the image as a blob, and give it a url for our images.
	    # image_uri is our our model attribute, -rhoblob indicates it is a blob attribute.
	    new_item = {'image_uri-rhoblob' => @baseurl+@bucket+'/'+File.basename(entry.key)}
	    puts  "Found: #{entry}"
	    @result[entry.key] = new_item
	  end
	  @result
	end

	def create(name_value_list)
	  puts "Creating: #{name_value_list.inspect}"   # When you do a sync,
	  name = name_value_list["image_uri"]     # the name-value list comes in as image_uri.
	  basename = name_value_list["filename"]  # the filename is the one on the Rhosync server.
	  # Store it locally on the file system.
	  AWS::S3::S3Object.store(
	    basename,
	    open(name),
	    @bucket,
	    :access => :public_read,
      :content_type => 'image/jpeg'
	  )
	  basename
  end

 #  def partition 
 #    lambda { self.user.username }
 #  end

 #  def self.rhoconnect_query(partition)
 #    @result={}
 #    AWS::S3::Bucket.find(@bucket).objects.each do |entry|
 #      # Get the image as a blob, and give it a url for our images.
 #      # image_uri is our our model attribute, -rhoblob indicates it is a blob attribute.
 #      new_item = {'image_uri-rhoblob' => @baseurl+@bucket+'/'+File.basename(entry.key)}
 #      puts  "Found: #{entry}"
 #      @result[entry.key] = new_item
 #    end
 #    @result
 #  end
end
