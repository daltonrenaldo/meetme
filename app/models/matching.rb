class Matching < ActiveRecord::Base
  include Rhoconnect::Resource

  attr_accessible :match_id, :status, :user_id
  belongs_to :user
  belongs_to :match, :class_name => "User"
  
  def partition
    lambda { self.user.username }
  end

  def self.rhoconnect_query(partition)
    user = User.find_by_email(partition)
    Matching.where("(user_id = ? OR match_id = ?) AND status != ?", user.id.to_s, user.id.to_s, "blocked")
  end
end
