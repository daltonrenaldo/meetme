class Interest < ActiveRecord::Base
  include Rhoconnect::Resource

  attr_accessible :name

  has_and_belongs_to_many :users

  def partition
    lambda { self.user.username }
  end

  def self.rhoconnect_query(partition)
    user = User.find_by_email(partition)
    user.interests
  end
end
