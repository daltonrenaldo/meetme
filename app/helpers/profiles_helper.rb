module ProfilesHelper
  def parse_date(date)
  	Date.strptime( date.gsub("/", " ").gsub("-", " ").gsub(".", " "), "%m %d %Y").to_s
  end
end
