# MeetMe

Author: Renaldo Pierre-Louis
Date: September 2, 2012

MeetMe is a social mobile application designed to facilate the encounter of people with similar interests. MeetMe aims to revolutionize the social media world, especially the dating aspect of it.

This is the server part of a mobile application. This is Being used as an API and the views are simply for admin purposes 

## Getting Started

1. First clone the repo.
2. Since MeetMe uses Ruby, Rails, and Rhodes technology, install them. (instructions can be found at their websites)

	### Linux
	------
	Follow theses instructions: http://blog.sudobits.com/2012/05/02/how-to-install-ruby-on-rails-in-ubuntu-12-04-lts/
	
	### Windows
	--------
	Download the Kit from: http://railsinstaller.org/
	
3. Navigate to the folder where the application resides:

    $ cd path_to_application

    $ bundle install --without production

    $ rake db:migrate

    $ rails server

4. Open favorite browser to http://localhost:3000 (note that this application is running on [meet-me-app.herokuapp.com](http://meet-me-app-herokuapp.com))
5. Create an account and have fun (by that I mean, see data being updated)

