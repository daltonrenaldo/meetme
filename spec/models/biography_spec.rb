require 'spec_helper'

describe Biography do
  specify "with valid data" do
  	FactoryGirl.build(:biography).should be_valid
  end

  context "when missing birthday" do
  	it { FactoryGirl.build(:biography, :birthday => "").should_not be_valid }
  	it do 
  	  lambda{ FactoryGirl.create(:biography, :birthday => "") }.should raise_error
  	end
  end
end
