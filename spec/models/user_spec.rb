require 'spec_helper'

describe User do
  context "Creating User" do
    it "should be valid" do
      FactoryGirl.build(:user).should be_valid
    end
    
    describe "user without username" do
      it "should not be valid" do
        FactoryGirl.build(:user, :username => "").should_not be_valid
      end

      it "should raise error" do
        lambda{ FactoryGirl.create(:user, :username => "") }.should raise_error
      end
    end

    describe "user without email" do
      it "should not be valid" do
        FactoryGirl.build(:user, :email => "").should_not be_valid
      end

      it "should raise error" do
        lambda{ FactoryGirl.create(:user, :email => "") }.should raise_error
      end
    end

    describe "user without password" do
      it "should not be valid" do
        FactoryGirl.build(:user, :password => "").should_not be_valid
      end

      it "should raise error" do
        lambda{ FactoryGirl.create(:user, :password => "") }.should raise_error
      end
    end

    describe "user without password_confirmation" do
      it "should not be valid" do
        FactoryGirl.build(:user, :password_confirmation => "").should_not be_valid
      end

      it "should raise error" do
        lambda{ FactoryGirl.create(:user, :password_confirmation => "") }.should raise_error
      end
    end

    describe "non matching password_confirmation" do
      it "should not be valid" do
        FactoryGirl.build(:user, :password => "meetme", :password_confirmation => "").should_not be_valid
      end

      it "should raise error" do
        lambda{ FactoryGirl.create(:user, :password => "meetme", :password_confirmation => "") }.should raise_error
      end
    end


    describe "user with duplicate identifier" do
      before do
        @user = FactoryGirl.create(:user)
      end
      
      after do
        user = User.find_by_username(@user.username)
        user.destroy
      end
      

      it "should not be valid if username is taken" do
        FactoryGirl.build(:user, :username => @user.username).should_not be_valid
      end

      it "should raiser error if username is take" do
        lambda{ FactoryGirl.create(:user, :username => @user.username) }.should raise_error
      end

      it "should not be valid if email is taken" do
        FactoryGirl.build(:user, :email => @user.email).should_not be_valid
      end

      it "should raiser error if email is taken" do
        lambda{ FactoryGirl.create(:user, :email => @user.email) }.should raise_error
      end
    end
  end
end
