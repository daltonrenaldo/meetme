require 'spec_helper'

describe "messages/edit" do
  before(:each) do
    @message = assign(:message, stub_model(Message,
      :user_id => 1,
      :recipient_id => 1,
      :message => "MyText",
      :user_deleted => false,
      :recipient_deleted => false
    ))
  end

  it "renders the edit message form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => messages_path(@message), :method => "post" do
      assert_select "input#message_user_id", :name => "message[user_id]"
      assert_select "input#message_recipient_id", :name => "message[recipient_id]"
      assert_select "textarea#message_message", :name => "message[message]"
      assert_select "input#message_user_deleted", :name => "message[user_deleted]"
      assert_select "input#message_recipient_deleted", :name => "message[recipient_deleted]"
    end
  end
end
