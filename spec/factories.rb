FactoryGirl.define do
  factory :user do
    username "foo"
    email "foobar@gmail.com"
    password "meetme"
    password_confirmation { password }
  end

  factory :biography do
    birthday DateTime.now.to_s
  end
end