class CreateBiographies < ActiveRecord::Migration
  def change
    create_table :biographies do |t|
      t.text :about
      t.date :birthday
      t.string :occupation
      t.string :photo
      t.references :user

      t.timestamps
    end
    add_index :biographies, :user_id
  end
end
