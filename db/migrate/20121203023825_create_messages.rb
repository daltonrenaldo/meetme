class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :user_id
      t.integer :recipient_id
      t.text :message
      t.boolean :user_deleted
      t.boolean :recipient_deleted

      t.timestamps
    end
  end
end
