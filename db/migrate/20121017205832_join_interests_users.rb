class JoinInterestsUsers < ActiveRecord::Migration
  def up
  	create_table 'interests_users', :id => false do |t|
    t.column :interest_id, :integer
    t.column :user_id, :integer
  end
  end

  def down
  	drop_table 'interests_users'
  end
end
