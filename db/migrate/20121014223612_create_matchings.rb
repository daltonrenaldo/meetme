class CreateMatchings < ActiveRecord::Migration
  def change
    create_table :matchings do |t|
      t.integer :user_id
      t.integer :match_id
      t.string :status

      t.timestamps
    end
  end
end
