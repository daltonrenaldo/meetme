class AddSlugToInterest < ActiveRecord::Migration
  def change
    add_column :interests, :slug, :string
  end
end
