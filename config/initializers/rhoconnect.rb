Rhoconnect.configure do |config|
  config.uri = "http://45022c1f584a4c0f8ab96bd49f910408@rhoserviced4e2f5e6.rhoconnect.com"
  config.app_endpoint = "http://meet-me-app.herokuapp.com"
  config.authenticate = lambda { |credential|
    user = User.find_by_email(credential['login'])
    if user
      user.authenticate(credential['password'])
    else
      false
    end
  }
end